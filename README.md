# Example-of-playing-audio-file-from-local-device-in-Android
private void playLocalAudio()throws Exception
{
   //The file is located in the /res/raw directory and called "music_file.mp3"
   mediaPLayer = MediaPlayer.create(this, R.raw.music_file);
   mediaPLayer.start();
}
